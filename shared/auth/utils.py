import random

# #############################################################################
#  Fonctions universelles
# #############################################################################


def generate_password(size=15):
    random.seed()
    alphabet = "abcdefghjkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789"
    password = ""
    for i in range(size):
        password += random.choice(alphabet)

    return password
