from django.urls import path

from . import views

urlpatterns = [
    path(
        "election/<int:election_id>/login",
        views.ElectionLoginView.as_view(),
        name="auth.election",
    ),
    path("pwd-create", views.CreatePwdAccount.as_view(), name="auth.create-account"),
    path("admin", views.AdminPanelView.as_view(), name="auth.admin"),
    path(
        "permissions", views.PermissionManagementView.as_view(), name="auth.permissions"
    ),
    path("accounts", views.AccountListView.as_view(), name="auth.accounts"),
    path("admins", views.AdminAccountsView.as_view(), name="auth.admins"),
]
