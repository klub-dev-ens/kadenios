from django.utils.translation import gettext_lazy as _

CONNECTION_METHODS = {
    "pwd": _("mot de passe"),
    "cas": _("CAS"),
}
