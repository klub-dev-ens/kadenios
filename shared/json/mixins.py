import json


class Serializer:
    serializable_fields = []

    def get_serializable_fields(self):
        return self.serializable_fields

    def to_json(self):
        data = {}

        for field in self.get_serializable_fields():
            if hasattr(self, field):
                data.update({field: getattr(self, field)})
            else:
                raise AttributeError(
                    "This object does not have a field named '{}'".format(field)
                )

        return json.dumps(data)
