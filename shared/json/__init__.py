from .mixins import Serializer  # noqa
from .views import JsonCreateView, JsonDeleteView, JsonDetailView, JsonUpdateView

__all__ = [
    "Serializer",
    "JsonCreateView",
    "JsonDeleteView",
    "JsonDetailView",
    "JsonUpdateView",
]
