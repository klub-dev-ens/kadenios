# #############################################################################
#  Fonctions universelles
# #############################################################################


def choices_length(choices):
    """Renvoie la longueur maximale des choix de choices"""
    m = 0
    for c in choices:
        m = max(m, len(c[0]))
    return m
