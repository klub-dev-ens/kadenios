from django.apps import apps
from django.conf import settings
from django.contrib import admin

if settings.DEBUG:
    models = apps.get_models()

    for model in models:
        try:
            admin.site.register(model)
        except admin.sites.AlreadyRegistered:
            pass
