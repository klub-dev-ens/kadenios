import markdown

from django import template

register = template.Library()


@register.filter(name="markdown")
def markdownify(text):
    return markdown.markdown(text, extensions=["extra"])
