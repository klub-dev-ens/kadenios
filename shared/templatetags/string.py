from django import template

register = template.Library()


@register.filter
def concatenate(a, b):
    return str(a) + str(b)
