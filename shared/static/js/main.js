const _$ = (s, e = document, a = true) => {
  const r = e.querySelectorAll(s) || [];
  if (!a) {
    return r.item(0);
  }
  return r;
};

const _id = s => document.getElementById(s);

const _get = (u, f) => {
  const xhr = new XMLHttpRequest();

  xhr.responseType = 'json';
  xhr.addEventListener('load', () => {
    f(xhr.response);
  });
  xhr.open('GET', u);
  xhr.send();
};

const _post = (u, d, f) => {
  const xhr = new XMLHttpRequest();
  const fd = new FormData(d);

  xhr.responseType = 'json';
  xhr.addEventListener('load', () => {
    f(xhr.response);
  });
  xhr.open('POST', u);
  xhr.send(fd);
};

const _notif = (m, c) => {
  const n = document.createElement('div');
  n.classList.add('notification', 'is-light');
  if (c !== undefined) {
    n.classList.add(`is-${c}`);
  }
  n.innerHTML = `${m}<button class="delete"></button>`;

  _id('notifications').insertBefore(n, _id('content'))

  _$('.delete', n, false).addEventListener('click', () => {
    n.remove();
  });
}

const _om = b => {
  b.addEventListener('click', () => {
    const m = _id(b.dataset.target);
    if ('post_url' in b.dataset) {
      _$('form', m, false).action = b.dataset.post_url;
    };

    if ('title' in b.dataset) {
      _$('.modal-card-title', m, false).innerHTML = b.dataset.title;
    };

    document.documentElement.classList.add('is-clipped');
    m.classList.add('is-active');
  });
}

const _cm = b => {
  b.addEventListener('click', () => {
    document.documentElement.classList.remove('is-clipped');
    _id(b.dataset.closes).classList.remove('is-active')
  });
}

const _sm = '.modal';
const _smb = '.modal-button';
const _smc = '.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button-close';

document.addEventListener('DOMContentLoaded', () => {
  // Delete notifications
  _$('.notification .delete').forEach(d => {
    const n = d.parentNode;

    d.addEventListener('click', () => {
      n.remove();
    });
  });

  // Interact with dropdowns
  const ds = _$('.dropdown:not(.is-hoverable)');

  ds.forEach(d => {
    d.addEventListener('click', e => {
      e.stopPropagation();
      d.classList.toggle('is-active');
    });
  });

  document.addEventListener('click', () => {
    ds.forEach(d => {
      d.classList.remove('is-active');
    });
  });

  // Interact with modals
  const ms = _$(_sm);
  const mbs = _$(_smb);
  const mcs = _$(_smc);

  mbs.forEach(_om);

  mcs.forEach(_cm);

  document.addEventListener('keydown', ev => {
    const e = ev || window.event;
    if (e.keyCode === 27) {
      ds.forEach(d => {
        d.classList.remove('is-active');
      });

      document.documentElement.classList.remove('is-clipped');

      ms.forEach(m => {
        m.classList.remove('is-active');
      });
    }
  });

  // Language selection
  _$('.dropdown-item.lang-selector').forEach(l => {
    l.addEventListener('click', () => {
      _id('lang-input').value = l.dataset.lang;
      _id('lang-form').submit();
    });
  });

  // Disable button after form submission
  _$('form').forEach(f => {
    f.addEventListener('submit', () => {
      _$('button[type=submit]', f).forEach(b => {
        b.classList.add('is-loading');
        setTimeout(() => {
          b.classList.remove('is-loading');
        }, 1000);
      });
    });
  });

  // Scroll to top button
  const up = _id('scroll-button');
  if (document.documentElement.scrollTop >= 100) {
    up.classList.remove('is-hidden');
  }

  window.onscroll = () => {
    if (document.documentElement.scrollTop >= 100) {
      up.classList.remove('is-hidden');
    } else {
      up.classList.add('is-hidden');
    }
  }

  up.addEventListener('click', () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  });

});
