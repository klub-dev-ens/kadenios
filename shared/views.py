from django.contrib import messages
from django.utils import timezone
from django.views.generic import RedirectView

# #############################################################################
#  Utils Views
# #############################################################################


class BackgroundUpdateView(RedirectView):
    success_message = ""

    def get_success_message(self):
        return self.success_message

    def get(self, request, *args, **kwargs):
        success_message = self.get_success_message()
        if success_message:
            messages.success(self.request, success_message)
        return super().get(request, *args, **kwargs)


class TimeMixin:
    def get_context_data(self, **kwargs):
        kwargs.update(current_time=timezone.now())
        return super().get_context_data(**kwargs)
