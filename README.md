# Kadenios

## Installation

L'installation se fait pour l'instant manuellement.

### Installation manuelle

Il est fortement conseillé d'utiliser un environnement virtuel pour Python.

Il vous faudra installer pip, les librairies de développement de python ainsi
que sqlite3, un moteur de base de données léger et simple d'utilisation. Sous
Debian et dérivées (Ubuntu, ...) :

    sudo apt-get install python3-pip python3-dev python3-venv sqlite3

Si vous décidez d'utiliser un environnement virtuel Python (virtualenv;
fortement conseillé), déplacez-vous dans le dossier où est installé kadenios
(le dossier où se trouve ce README), et créez-le maintenant :

    python3 -m venv venv

Pour l'activer, il faut taper

    . venv/bin/activate

depuis le même dossier.

Une autre solution est d'utiliser [`pyenv`](https://github.com/pyenv/pyenv) et
[`pyenv-virtualenv`](https://github.com/pyenv/pyenv-virtualenv).

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 kadenios
    pyenv local kadenios

Vous pouvez maintenant installer les dépendances Python depuis le fichier
`requirements-dev.txt` :

    pip install -U pip
    pip install -r requirements-dev.txt

Nous avons un git hook de pre-commit pour formatter et vérifier que votre code
vérifie nos conventions. Pour bénéficier des mises à jour du hook, préférez
encore l'installation *via* un lien symbolique:

    ln -s ../../.pre-commit.sh .git/hooks/pre-commit

#### Fin d'installation

Il ne vous reste plus qu'à initialiser les modèles de Django :

    ./manage.py migrate

Il vous faut ensuite créer un superutilisateur :

    ./manage.py createadmin {username} {password} --superuser

Vous êtes prêts à développer ! Lancer Kadenios en faisant

    ./manage.py runserver

## Fonctionnalités

### Implémentées

- Posibilité de créer des élections, ainsi que de les administrer
- Système de vote modulaire, on a des questions de type condorcet, uninominal ou par assentiment
- Un début d'Access Control

### TODO

- Access Control complet
