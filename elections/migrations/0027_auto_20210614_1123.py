# Generated by Django 3.2.4 on 2021-06-14 09:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("elections", "0026_auto_20210529_2246"),
    ]

    operations = [
        migrations.AlterField(
            model_name="question",
            name="text_en",
            field=models.TextField(blank=True, default="", verbose_name="question"),
        ),
        migrations.AlterField(
            model_name="question",
            name="text_fr",
            field=models.TextField(blank=True, default="", verbose_name="question"),
        ),
    ]
