# Generated by Django 2.2.19 on 2021-04-04 16:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("elections", "0018_auto_20210331_1317"),
    ]

    operations = [
        migrations.AddField(
            model_name="option",
            name="winner",
            field=models.BooleanField(default=False, verbose_name="option gagnante"),
        ),
    ]
