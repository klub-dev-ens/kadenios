# Generated by Django 2.2.17 on 2020-12-23 19:33

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("elections", "0005_user_full_name"),
    ]

    operations = [
        migrations.AddField(
            model_name="election",
            name="sent_mail",
            field=models.BooleanField(
                default=False, verbose_name="mail avec les identifiants envoyé"
            ),
        ),
    ]
