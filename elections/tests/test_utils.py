from datetime import timedelta

from django.utils import timezone

from elections.models import Election


def create_election(i, restricted=True, creator=None, future=False):
    now = timezone.now()
    day = timedelta(days=1)

    if future:
        start_date = now + day
        end_date = now + 2 * day
    else:
        start_date = now - day
        end_date = now + day

    return Election.objects.create(
        name=f"Election {i}",
        short_name=f"election_{i}",
        start_date=start_date,
        end_date=end_date,
        restricted=restricted,
        created_by=creator,
    )
