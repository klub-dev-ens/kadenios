from django.utils.translation import gettext_lazy as _

MAIL_VOTERS = (
    "Dear {full_name},\n"
    "\n"
    "\n"
    "Election URL: {election_url}\n"
    "The election will take place from {start} to {end}.\n"
    "\n"
    "Your voter ID: {username}\n"
    "Your password: {password}\n"
    "\n"
    "-- \n"
    "Kadenios"
)

MAIL_VOTE_DELETED = (
    "Dear {full_name},\n"
    "\n"
    "Your vote for {election_name} has been removed."
    "\n"
    "-- \n"
    "Kadenios"
)

QUESTION_TYPES = [
    ("assentiment", _("Assentiment")),
    ("uninominal", _("Uninominal")),
    ("condorcet", _("Condorcet")),
]

BALLOT_TYPE = {
    "assentiment": "select",
    "uninominal": "select",
    "condorcet": "rank",
}

VOTE_RULES = {
    "assentiment": _(
        "Le mode de scrutin pour cette question est un vote par assentiment. "
        "Vous pouvez donc sélectionner autant d'options que vous souhaitez. "
        "Vous pouvez également ne sélectionner aucune option."
    ),
    "uninominal": _(
        "Le mode de scrutin pour cette question est un vote uninominal. "
        "Vous ne pouvez donc sélectionner qu'une seule option."
    ),
    "condorcet": _(
        "Le mode de scrutin pour cette question est un vote de type condorcet. "
        "Vous devez classer les options <b>entre 1 et {nb_options}</b>, l'option "
        "classée 1 étant votre préférée. <b>Vous pouvez donner le même classement "
        "à plusieurs options</b>, si vous laissez vide le classement d'une option, "
        "elle sera classée dernière automatiquement."
    ),
}

CAST_FUNCTIONS = {
    "assentiment": "cast_select",
    "uninominal": "cast_select",
    "condorcet": "cast_rank",
}

TALLY_FUNCTIONS = {
    "assentiment": "tally_select",
    "uninominal": "tally_select",
    "condorcet": "tally_schultze",
}

VALIDATE_FUNCTIONS = {
    "assentiment": "always_true",
    "uninominal": "unique_selected",
    "condorcet": "limit_ranks",
}
