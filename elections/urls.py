from django.urls import path

from . import views

urlpatterns = [
    # Admin views
    path("create/", views.ElectionCreateView.as_view(), name="election.create"),
    path("admin/<int:pk>", views.ElectionAdminView.as_view(), name="election.admin"),
    path(
        "mail-voters/<int:pk>",
        views.ElectionMailVotersView.as_view(),
        name="election.mail-voters",
    ),
    path(
        "upload-voters/<int:pk>",
        views.ElectionUploadVotersView.as_view(),
        name="election.upload-voters",
    ),
    path(
        "export-voters/<int:pk>",
        views.ExportVotersView.as_view(),
        name="election.export-voters",
    ),
    path(
        "results/<int:pk>",
        views.DownloadResultsView.as_view(),
        name="election.download-results",
    ),
    path(
        "delete-vote/<int:pk>/<int:user_pk>/<int:anchor>",
        views.DeleteVoteView.as_view(),
        name="election.delete-vote",
    ),
    path(
        "visible/<int:pk>",
        views.ElectionSetVisibleView.as_view(),
        name="election.set-visible",
    ),
    path("update/<int:pk>", views.ElectionUpdateView.as_view(), name="election.update"),
    path("tally/<int:pk>", views.ElectionTallyView.as_view(), name="election.tally"),
    path(
        "publish/<int:pk>",
        views.ElectionChangePublicationView.as_view(),
        name="election.publish",
    ),
    path(
        "archive/<int:pk>", views.ElectionArchiveView.as_view(), name="election.archive"
    ),
    # Question views
    path(
        "add-question/<int:pk>",
        views.CreateQuestionView.as_view(),
        name="election.add-question",
    ),
    path(
        "mod-question/<int:pk>",
        views.UpdateQuestionView.as_view(),
        name="election.mod-question",
    ),
    path(
        "del-question/<int:pk>",
        views.DeleteQuestionView.as_view(),
        name="election.del-question",
    ),
    # Option views
    path(
        "add-option/<int:pk>",
        views.CreateOptionView.as_view(),
        name="election.add-option",
    ),
    path(
        "mod-option/<int:pk>",
        views.UpdateOptionView.as_view(),
        name="election.mod-option",
    ),
    path(
        "del-option/<int:pk>",
        views.DeleteOptionView.as_view(),
        name="election.del-option",
    ),
    # Common views
    path("", views.ElectionListView.as_view(), name="election.list"),
    path("view/<int:pk>", views.ElectionView.as_view(), name="election.view"),
    path(
        "view/<int:pk>/voters",
        views.ElectionVotersView.as_view(),
        name="election.voters",
    ),
    path(
        "view/<int:pk>/ballots",
        views.ElectionBallotsView.as_view(),
        name="election.ballots",
    ),
    path("vote/<int:pk>", views.VoteView.as_view(), name="election.vote"),
]
