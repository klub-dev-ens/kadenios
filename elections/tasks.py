from background_task import background

from .models import Election
from .utils import send_mail


@background
def send_election_mail(election_pk, subject, body, reply_to):
    election = Election.objects.get(pk=election_pk)
    send_mail(election, subject, body, reply_to)
    election.sent_mail = True
    election.save(update_fields=["sent_mail"])
