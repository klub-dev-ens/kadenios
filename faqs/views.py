from django.contrib.messages.views import SuccessMessageMixin
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DetailView, ListView, UpdateView

from .forms import FaqForm
from .mixins import AdminOnlyMixin, CreatorOnlyMixin
from .models import Faq

# #############################################################################
#  Administration Views
# #############################################################################


class FaqCreateView(AdminOnlyMixin, SuccessMessageMixin, CreateView):
    model = Faq
    form_class = FaqForm
    success_message = _("Faq créée avec succès !")
    template_name = "faqs/faq_create.html"

    def get_success_url(self):
        return reverse("faq.view", args=[self.object.anchor])

    def form_valid(self, form):
        form.instance.author = self.request.user

        return super().form_valid(form)


class FaqEditView(CreatorOnlyMixin, SuccessMessageMixin, UpdateView):
    model = Faq
    form_class = FaqForm
    slug_field = "anchor"
    success_message = _("Faq modifiée avec succès !")
    template_name = "faqs/faq_edit.html"

    def get_success_url(self):
        return reverse("faq.view", args=[self.object.anchor])


# #############################################################################
#  Public Views
# #############################################################################


class FaqListView(ListView):
    model = Faq
    template_name = "faqs/faq_list.html"


class FaqView(DetailView):
    model = Faq
    template_name = "faqs/faq.html"
    slug_field = "anchor"
