from translated_fields import TranslatedFieldWithFallback

from django.contrib.auth import get_user_model
from django.db import models
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class Faq(models.Model):
    title = TranslatedFieldWithFallback(
        models.CharField(_("titre"), blank=False, max_length=255)
    )
    description = TranslatedFieldWithFallback(
        models.TextField(_("description"), blank=False)
    )
    content = TranslatedFieldWithFallback(models.TextField(_("contenu"), blank=True))

    author = models.ForeignKey(
        User, related_name="faqs", null=True, on_delete=models.SET_NULL
    )
    last_modified = models.DateField(_("mise à jour"), auto_now=True)

    anchor = models.CharField(_("ancre"), max_length=20)

    class Meta:
        permissions = [
            ("faq_admin", "Can create faqs"),
        ]
        constraints = [
            models.UniqueConstraint(fields=["anchor"], name="unique_faq_anchor")
        ]
