from django.contrib.auth.mixins import PermissionRequiredMixin


class AdminOnlyMixin(PermissionRequiredMixin):
    """Restreint l'accès aux admins"""

    permission_required = "faqs.faq_admin"


class CreatorOnlyMixin(AdminOnlyMixin):
    """Restreint l'accès à l'auteur"""

    def get_queryset(self):
        return super().get_queryset().filter(author=self.request.user)
