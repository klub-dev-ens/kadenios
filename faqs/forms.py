from translated_fields import language_code_formfield_callback

from django import forms

from .models import Faq


class FaqForm(forms.ModelForm):
    formfield_callback = language_code_formfield_callback

    class Meta:
        model = Faq
        fields = [
            *Faq.title.fields,
            "anchor",
            *Faq.description.fields,
            *Faq.content.fields,
        ]
        widgets = {
            "description_en": forms.Textarea(
                attrs={"rows": 4, "class": "is-family-monospace"}
            ),
            "description_fr": forms.Textarea(
                attrs={"rows": 4, "class": "is-family-monospace"}
            ),
            "content_en": forms.Textarea(attrs={"class": "is-family-monospace"}),
            "content_fr": forms.Textarea(attrs={"class": "is-family-monospace"}),
        }
