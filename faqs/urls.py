from django.urls import path

from . import views

urlpatterns = [
    # Admin views
    path("create", views.FaqCreateView.as_view(), name="faq.create"),
    path("edit/<slug:slug>", views.FaqEditView.as_view(), name="faq.edit"),
    # Public views
    path("", views.FaqListView.as_view(), name="faq.list"),
    path("view/<slug:slug>", views.FaqView.as_view(), name="faq.view"),
]
