let
  mach-nix = import
    (builtins.fetchGit {
      url = "https://github.com/DavHau/mach-nix";
      ref = "refs/tags/3.5.0";
    })
    { };

  requirements = builtins.readFile ./requirements.txt;

  requirements-dev = ''
    django-debug-toolbar
    ipython
    black
    isort
    flake8
  '';
in

mach-nix.mkPythonShell {
  requirements = requirements + requirements-dev;
}
