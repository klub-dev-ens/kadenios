"""
Paramètres communs entre dev et prod
"""

import os
import sys

from django.urls import reverse_lazy
from django.utils.translation import gettext_lazy as _

# #############################################################################
#  Secrets
# #############################################################################

try:
    from . import secret
except ImportError:
    raise ImportError(
        "The secret.py file is missing.\n"
        "For a development environment, simply copy secret_example.py"
    )


def import_secret(name):
    """
    Shorthand for importing a value from the secret module and raising an
    informative exception if a secret is missing.
    """
    try:
        return getattr(secret, name)
    except AttributeError:
        raise RuntimeError("Secret missing: {}".format(name))


SECRET_KEY = import_secret("SECRET_KEY")
ADMINS = import_secret("ADMINS")
SERVER_EMAIL = import_secret("SERVER_EMAIL")
EMAIL_HOST = import_secret("EMAIL_HOST")


# #############################################################################
#  Paramètres par défaut pour Django
# #############################################################################

DEBUG = False
TESTING = len(sys.argv) > 1 and sys.argv[1] == "test"
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "kadenios.apps.IgnoreSrcStaticFilesConfig",
    "background_task",
    "shared",
    "elections",
    "faqs",
    "authens",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "kadenios.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "kadenios.wsgi.application"

DEFAULT_AUTO_FIELD = "django.db.models.AutoField"

DEFAULT_FROM_EMAIL = "Kadenios <cof-geek-sysadmin@ens.fr>"

# #############################################################################
#  Paramètres d'authentification
# #############################################################################

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

AUTH_USER_MODEL = "elections.User"
AUTHENTICATION_BACKENDS = [
    "shared.auth.backends.PwdBackend",
    "shared.auth.backends.CASBackend",
    "shared.auth.backends.ElectionBackend",
]

LOGIN_URL = reverse_lazy("authens:login")
LOGIN_REDIRECT_URL = "/"

AUTHENS_USE_OLDCAS = False

# #############################################################################
#  Paramètres de langage
# #############################################################################

LANGUAGE_CODE = "fr-fr"
TIME_ZONE = "Europe/Paris"

USE_I18N = True
USE_L10N = True
USE_TZ = True

LANGUAGES = [
    ("fr", _("Français")),
    ("en", _("Anglais")),
]

LOCALE_PATHS = [os.path.join(BASE_DIR, "shared", "locale")]

# #############################################################################
#  Paramètres des fichiers statiques
# #############################################################################

STATIC_URL = "/static/"
