"""
Paramètres pour la mise en production
"""

import os

from .common import *  # noqa
from .common import BASE_DIR, import_secret

# #############################################################################
#  Secrets de production
# #############################################################################

REDIS_PASSWD = import_secret("REDIS_PASSWD")
REDIS_DB = import_secret("REDIS_DB")
REDIS_HOST = import_secret("REDIS_HOST")
REDIS_PORT = import_secret("REDIS_PORT")

DBNAME = import_secret("DBNAME")
DBUSER = import_secret("DBUSER")
DBPASSWD = import_secret("DBPASSWD")

# #############################################################################
#  À modifier possiblement lors de la mise en production
# #############################################################################

ALLOWED_HOSTS = ["vote.eleves.ens.fr"]

STATIC_ROOT = os.path.join(os.path.dirname(BASE_DIR), "static")

SERVER_EMAIL = "kadenios@www.eleves.ens.fr"

# #############################################################################
#  Paramètres du cache
# #############################################################################

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://:{passwd}@{host}:{port}/{db}".format(
            passwd=REDIS_PASSWD, host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB
        ),
    }
}

# #############################################################################
#  Paramètres de la base de données
# #############################################################################

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": DBNAME,
        "USER": DBUSER,
        "PASSWORD": DBPASSWD,
        "HOST": os.environ.get("DBHOST", ""),
    }
}

# #############################################################################
#  Paramètres Https
# #############################################################################

SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
SECURE_SSL_REDIRECT = True

SECURE_HSTS_SECONDS = 31536000
SECURE_HSTS_PRELOAD = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
