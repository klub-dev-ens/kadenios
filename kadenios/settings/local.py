"""
Paramètre pour le développement local
"""

import os

from .common import *  # noqa
from .common import BASE_DIR, INSTALLED_APPS, MIDDLEWARE, TESTING

# #############################################################################
#  Paramètres Django
# #############################################################################

ALLOWED_HOSTS = []

DEBUG = True
EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

STATIC_URL = "/static/"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": os.path.join(BASE_DIR, "db.sqlite3"),
    }
}

# Use the default cache backend for local development
CACHES = {"default": {"BACKEND": "django.core.cache.backends.locmem.LocMemCache"}}

# Pas besoin de sécurité en local
AUTH_PASSWORD_VALIDATORS = []
PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

# #############################################################################
#  Paramètres pour la Django Debug Toolbar
# #############################################################################


def show_toolbar(request):
    """
    On active la debug-toolbar en mode développement local sauf :
    - dans l'admin où ça ne sert pas à grand chose;
    - si la variable d'environnement DJANGO_NO_DDT est à 1 → ça permet de la désactiver
      sans modifier ce fichier en exécutant `export DJANGO_NO_DDT=1` dans le terminal
      qui lance `./manage.py runserver`.
    """
    env_no_ddt = bool(os.environ.get("DJANGO_NO_DDT", None))
    return DEBUG and not env_no_ddt and not request.path.startswith("/admin/")


if not TESTING:
    INSTALLED_APPS = INSTALLED_APPS + ["debug_toolbar"]
    MIDDLEWARE = ["debug_toolbar.middleware.DebugToolbarMiddleware"] + MIDDLEWARE
    DEBUG_TOOLBAR_CONFIG = {"SHOW_TOOLBAR_CALLBACK": show_toolbar}
