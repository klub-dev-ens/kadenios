from django.conf import settings
from django.contrib import admin
from django.urls import include, path

from .views import HomeView

urlpatterns = [
    path("", HomeView.as_view(), name="kadenios"),
    path("elections/", include("elections.urls")),
    path("faqs/", include("faqs.urls")),
    path("auth/", include("shared.auth.urls")),
    path("authens/", include("authens.urls")),
    path("i18n/", include("django.conf.urls.i18n")),
]

if settings.DEBUG:
    urlpatterns += [
        path("admin/", admin.site.urls),
    ]

if "debug_toolbar" in settings.INSTALLED_APPS:
    from debug_toolbar import urls as djdt_urls

    urlpatterns += [
        path("__debug__/", include(djdt_urls)),
    ]
